+++
title = "Fellesskap"
+++

Denne siden forsøker å forklare hvordan fellesskapet rundt Redox OS er organisert samt hjelpe det med å navigere det.


<a id="chat"></a>
## [Chat](https://matrix.to/#/#redox:matrix.org)

<a id="forum"></a>
## [Forum](https://discourse.redox-os.org/)

Dette er den beste måten å diskutere genrelle temaer rundt Redox OS. Du kan melde deg inn likt med hvilket som helst annet nettsamfunn.

<a id="gitlab"></a>
## [GitLab](https://gitlab.redox-os.org/redox-os/redox)


En noe mer formell måte å kommunisere med andre Redox-utviklere, ikke fult så rask og hendig som chatten. Legg inn en issue hvis du får problemer med kompilering og testing, eller om du vil diskutere et spesifikt emne / tema; for eksempel funksjoner, egenskaper, programmeringsstil, inkonsistenser i programmeringen, små endringer, feilrettinger osv.

<a id="reddit"></a>
## [Redox OS på Reddit](https://www.reddit.com/r/Redox/)

Hvis du ønsker å lese om hva som foregår for tiden og prate om det.

[reddit.com/r/rust](https://www.reddit.com/r/rust) for nyheter og diskusjoner knyttet til programmeringsspråket Rust.
