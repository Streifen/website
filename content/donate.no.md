+++
title = "Donere/Merch"
+++

## Redox OS Nonprofit

Redox OS har et Colorado (US) incorporated 501(c)(4) nonprofit som forvalter donasjoner. Donasjoner til Redox OS nonprofit blir brukt etter føringer fra Redox OS volunteer board of directors.

Du kan donere til Redox OS på følgende vis:
 - [Patreon](https://www.patreon.com/redox_os)
 - [Donorbox](https://donorbox.org/redox-os)
 - for flere doneringsalterantiv og -muligheter, send en forespørsel til donate@redox-os.org

## Merch / Støttemateriell / Profilering

Vi selger T-skjorter på Teespring, du kan kjøre de [her](https://redox-os.creator-spring.com/).

Hvert salg er en støtte til Redox OS Nonprofit.

## Jeremy Soller

Jeremy Soller er skaper, vedlikeholder og ledende utvikler av Redox OS.
Donasjoner og gaver til han blir behandlet som skattbare gaver og vil bli brukt til formål han selv velger.

Du kan donere til han på følgende vis:

- [Liberapay](https://liberapay.com/redox_os)
- [Paypal](https://www.paypal.me/redoxos)
- Jeremy tar ikke lenger i mot Bitcoin eller Etherun donasjoner. Ikke gi donasjoner til adresser som tidligere var oppgitt på websiden, da de ikke vil nå frem.

Følgende givere har donert $4 eller mer til Jeremy som bidrag til utviklingen av Redox OS:
{{% partial "donors/jackpot51.html" %}}
