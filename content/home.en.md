+++
title = "Home"
url = "home"
+++

<!--Two-Space indentation is necessary here 🤮🤮 -->

<div id="sponsors">
  <h1>Sponsors</h1>
  <p>Without the generous and continued support of our sponsors, continuing the development of Redox OS is very challenging. We are extremely grateful to each sponsor mentioned below.</p>
  
  <div class="row">
    <p>No one here yet <a href="/donate">Be the first!</a></p>
  </div>
  
  <a href="./donate"><button class="donate">Donate</button></a>
  
  <h2>Individual sponsors</h2>
  <p>To this list of course also belong the individuals who have contributed donations to the Redox OS project.</p>
</div>

<hr />

<div>
  <h1>What is Redox</h1>
  
  <p>Redox is a free, open-source operating system written Rust.</p>
  <p>Using a microkernel design, allows us to focus on safety, security, and stability.</p>
  
  <a href="#learn-more"><button class="external">Learn More</button></a>
  <a href="https://gitlab.redox-os.org/redox-os/redox/-/blob/master/CONTRIBUTING.md"><button class="gitlab">Jump In!</button></a>
  <a href="/donate"><button class="donate">Donate</button></a>
</div>

<div class="advantage">
  <h1 id="learn-more">Guarded with Rust</h1>
  <h2>Effectively eliminates bugs</h2>
  <p>Rust's compile-time safety and restrictive syntax effectively eliminate memory bugs.</p>

  <div class="card">
    <h2>More resillient to Data Corruption</h2>
    <article>
      <p>The Rust compiler makes it almost impossible to corrupt memory, making Redox much more reliable and stable.</p>
    </article>
  </div>
  
  <h2>Better Safety, better Security</h2>
  <p>Memory security issues can make up 70% of vulenrabilities<a><sup>2</sup></a>. Rust prevents these issues, making Redox safer and more secure by design.</p>
  
  <div class="card">
    <h2>Keeping multiple tasks safe</h2>
    <article>
      <p>
        C/C++ can have trouble with data when running multiple tasks at once. 
        This can cause sneaky issues or dangers. 
        Rust avoids these issues by verifying the data before it's used or changed.
      </p>
    </article>
  </div>
</div>

<div class="advantage">
  <h1>A Microkernel Architecture</h1>
  <div class="card">
    <h2>More security by less privilege</h2>
    <article>
      <p>
        A microkernel is the near-minimum amount of software that can provide the mechanisms needed to implement an operating system, 
        which runs on the highest privilege of the processor.
      </p>
    </article>
  </div>
  
  <div class="card">
    <h2>Stability with bug isolation</h2>
    <article>
      <p>
        The system components that run in user-space are isolated from the system kernel. 
        Because of this, most bugs won't affect and crash the whole system.
      </p>
    </article>
  </div>
  
  <div class="card">
    <h2>Modularity and restartless design</h2>
    <article>
      <p>
        A stable microkernel changes little and reboots are rare. Most system parts in Redox are in user-space, 
        so they can be replaced on the fly, and no system reboot is required.
      </p>
    </article>
  </div>
  
  <div class="card">
    <h2>Ease to develop and debug</h2>
    <article>
      <p>Redox makes software development and improvement easer. System parts are separate from the microkernel, simplifying testing and debugging.</p>
    </article>
  </div>
</div>

<div class="advantage">
  <h1>Linux apps on Redox</h1>
  <div class="card">
    <h2>Relibc: Redox OS's Linux bridge</h2>
    <article>
      <p>Relibc is a special library that enables Redox OS to run may Linux apps. It acts as a brige between the language of Linux apps and Redox OS.</p>
    </article>
  </div>
  
  <div class="card">
    <h2>Growing in compatibility with Linux Apps</h2>
    <article>
      <p>Currenty, dozens of programs and many more libraries work effortlessly. Over 1 000 programs and libraries are currently being ported to Redox.</p>
    </article>
  </div>
</div>

<div class="advantage">
  <h1>Open Source and Free</h1>
  
  <div class="card">
    <h2>Run any software freely</h2>
    <article>
      <p>Use Redox OS and run any compatible software for any purpose, without worrying about source or licensing issues.</p>
    </article>
  </div>

  <div class="card">
    <h2>Fix errors or report them</h2>
    <article>
      <p>Anyone can check the quality and security of Redox's source code, imprpove it, or report bugs problems to the devs.</p>
    </article>
  </div>
  
  <div class="card">
    <h2>Free and always will be</h2>
    <article>
      <p>No companies own or control Redox OS. Redox OS is a nonprofit project that runs on grants and donations.</p>
    </article>
  </div>
</div>

<!-- Latest News -->

<hr />

<h1>Latest News</h1>

<p>Nothing here yet</p>
