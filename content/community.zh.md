+++
title = "社区"
+++

通过本页面, 你能够了解 Redox OS 社区的构成, 它将帮助你导航.
点击标题进入相应页面.

我们在所有的社区/聊天频道中都遵守 [Rust 行为准则](https://www.rust-lang.org/policies/code-of-conduct)

## [公告](https://matrix.to/#/#redox-announcements:matrix.org)

我们在 [这个](https://matrix.to/#/#redox-announcements:matrix.org) Matrix 房间中发布公告. 它是公开的,你无需登录 Matrix 即可阅读

- #redox-announcements:matrix.org (如果你不想使用外部 Matrix 链接,请使用此 Matrix 房间地址)

## [聊天](https://matrix.to/#/#redox-join:matrix.org)

Matrix 是与 Redox OS 团队和社区交流的官方方式(这些房间仅提供英语,我们不接受其他语言,因为我们不懂这些语言).

Matrix 有几个不同的客户端. [Element](https://element.io/) 是一种常用的选择,它适用于 Web 浏览器、Linux、MacOSX、Windows、Android 和 iOS.

如果你使用 Element 时出现了问题, 可以尝试 [nheko](https://nheko-reborn.github.io/).

- 加入 [这个](https://matrix.to/#/#redox-join:matrix.org) 房间, 别忘了申请 Redox Matrix 空间 的加入请求.
- #redox-join:matrix.org (如果你不想使用外部 Matrix 链接,请使用此 Matrix 房间地址)

(我们建议你在进入 Redox 空间后离开“加入请求”房间)

如果你想在我们的房间中进行大型讨论,你应该使用 Element 主题(Thread),如果同一房间内发生更多讨论,它更有组织性并且易于跟踪.

你可以在 [这个](https://doc.redox-os.org/book/ch13-01-chat.html) 页面获得更多信息.

## [Summer of Code](/rsoc)

**Redox Summer of Code** (RSoC) 计划每年在资金允许的情况下运行,我们可能会参加其他 Summer of Code 类型的计划.
我们的 Summer of Code 计划的该书和今年的计划请参见 [这里](/rsoc).
查看我们的 [RSoC 提议指导](/rsoc-proposal-how-to) and [项目建议](/rsoc-project-suggestions).

## [GitLab](https://gitlab.redox-os.org/redox-os/redox)

与其他 Redox 开发人员沟通的一种稍微正式的方式,但不像聊天那样快速和方便. 当你遇到编译或测试问题时提交问题. 如果你想讨论某个主题,也可以使用问题：功能、代码风格、代码不一致、微小的更改和修复等.

如果你希望注册一个账号, 阅读 [这个页面](https://doc.redox-os.org/book/ch12-01-signing-in-to-gitlab.html).

如果你有准备好的 MR（合并请求）,你必须在 ["MR"](https://matrix.to/#/#redox-mrs:matrix.org) 房间中发送链接. 要加入此房间,你需要在 ["Join Requests”](https://matrix.to/#/#redox-join:matrix.org) 房间中请求邀请.

通过在房间中发送消息,你的MR将不会被遗忘或积累冲突.

## [Lemmy](https://lemmy.world/c/redox)

我们的 Reddit 替代方案,我们在 Reddit 上发布新闻和社区话题.

## [Reddit](https://www.reddit.com/r/Redox/)

如果你想快速了解正在发生的事情并进行讨论.

通过 [reddit.com/r/rust](https://www.reddit.com/r/rust) 获得相关的 Rust 新闻和进行讨论.

## [Fosstodon](https://fosstodon.org/@redox)

我们的 Twitter 替代方案,我们在 Twitter 上发布新闻和社区话题.

## [Twitter](https://twitter.com/redox_os)

新闻和社区主题.

## [YouTube](https://www.youtube.com/@RedoxOS)

演示和董事会会议.

## [Forum](https://discourse.redox-os.org/)

这是我们的存档论坛,包含旧/经典问题,它处于非活动状态,只能用于存档目的. 如果你有疑问,请通过 Matrix 聊天发送.

## [Talks](/talks/)

各种活动和会议中的 Redox Talks.

## Spread the word

社区外展是 Redox 成功的重要组成部分. 如果更多的人了解 Redox,那么可能会有更多的贡献者介入,每个人都可以从他们增加的专业知识中受益. 你可以通过撰写文章、与其他操作系统爱好者交谈或寻找有兴趣了解更多有关 Redox 的社区来发挥作用.